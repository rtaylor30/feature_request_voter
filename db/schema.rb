# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140605082908) do

  create_table "ticket_replies", force: true do |t|
    t.integer  "ticket_id"
    t.text     "description"
    t.datetime "created_at"
    t.integer  "created_by_id"
    t.datetime "updated_at"
  end

  add_index "ticket_replies", ["created_by_id"], name: "index_ticket_replies_on_created_by_id"
  add_index "ticket_replies", ["ticket_id"], name: "index_ticket_replies_on_ticket_id"

  create_table "ticket_user_up_votes", force: true do |t|
    t.integer  "ticket_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ticket_user_up_votes", ["ticket_id"], name: "index_ticket_user_up_votes_on_ticket_id"
  add_index "ticket_user_up_votes", ["user_id"], name: "index_ticket_user_up_votes_on_user_id"

  create_table "tickets", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "estimated_delivery"
    t.string   "status"
    t.string   "jira_ticket_number"
    t.datetime "created_at"
    t.integer  "created_by_id"
    t.boolean  "active"
    t.string   "epic"
    t.datetime "updated_at"
  end

  add_index "tickets", ["created_by_id"], name: "index_tickets_on_created_by_id"

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
