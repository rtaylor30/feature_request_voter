class CreateTicketUserUpVotes < ActiveRecord::Migration
  def change
    create_table :ticket_user_up_votes do |t|
      t.references :ticket, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
