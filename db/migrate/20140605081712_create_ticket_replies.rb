class CreateTicketReplies < ActiveRecord::Migration
  def change
    create_table :ticket_replies do |t|
      t.references :ticket, index: true
      t.text :description
      t.datetime :created_at
      t.references :created_by, index: true

      t.timestamps
    end
  end
end
