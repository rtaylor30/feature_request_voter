class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :title
      t.text :description
      t.datetime :estimated_delivery
      t.string :status
      t.string :jira_ticket_number
      t.datetime :created_at
      t.references :created_by, index: true
      t.boolean :active
      t.string :epic

      t.timestamps
    end
  end
end
