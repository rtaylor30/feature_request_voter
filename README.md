Feature Request Voting System
=============================

This is a simple Ruby on Rails application that can hold feature requests. The users of the system
can vote on the features that they think are useful.

