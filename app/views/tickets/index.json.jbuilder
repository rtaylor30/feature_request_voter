json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :id, :title, :description, :estimated_delivery, :status, :jira_ticket_number, :created_at, :created_by_id, :active, :epic
  json.url ticket_url(ticket, format: :json)
end
