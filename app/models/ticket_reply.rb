class TicketReply < ActiveRecord::Base
  belongs_to :ticket
  belongs_to :created_by, :class_name => :User
end
