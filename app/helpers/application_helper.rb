module ApplicationHelper
  module BootstrapExtension
  
    def form_for( resource, options = {} )
      add_form_control_to options
      super
    end
    
    def text_area( resource, column, options = {} )
      add_form_control_to options
      super
    end
    
    def text_field( resource, column, options = {} )
      add_form_control_to options
      super
    end
    
    def text_field_tag( name, options = {} )
      add_form_control_to options
      super
    end
    
    def primary_button( text, options = {} )
      cls = options[ :class ]
      if cls.nil?
        cls = 'class="btn btn-primary"'
      else
        cls = 'class="' + cls + ' btn btn-primary"'
      end
      
      
      "<button #{cls}>#{html_escape text}</button>".html_safe
    end
    
    private
    
    def add_form_control_to( options )
      if options[ :class ].nil?
        options[ :class ] = 'form-control'
      else
        options[ :class ] << ' form-control'
      end
    end
  end
  
  include BootstrapExtension
end
